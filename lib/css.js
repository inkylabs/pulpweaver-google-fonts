const googleFonts = require('@inkylabs/google-fonts')

// BEGIN css utilities ////////////////////////////////////////////////////////

async function generateCss (fonts) {
  const units = []
  for (let font of fonts) {
    const ttf = await googleFonts.relPath(font)
    units.push(`@font-face {
  font-family: '${font}';
  src: url('/module/google-fonts/fonts/${ttf}') format('truetype');
}`)
  }
  return units.join('\n')
}

// BEGIN exports //////////////////////////////////////////////////////////////

module.exports = {
  generateCss
}
