/* eslint-env jest */
const { generateCss } = require('./css')

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('generateCss', () => {
  it('two styles loaded', async () => {
    const css = await generateCss(['cardo', 'cormorantgaramond'])
    expect(css).toEqual(`@font-face {
  font-family: 'cardo';
  src: url('/module/google-fonts/fonts/ofl/cardo/Cardo-Regular.ttf') format('truetype');
}
@font-face {
  font-family: 'cormorantgaramond';
  src: url('/module/google-fonts/fonts/ofl/cormorantgaramond/CormorantGaramond-Regular.ttf') format('truetype');
}`)
  })
})
