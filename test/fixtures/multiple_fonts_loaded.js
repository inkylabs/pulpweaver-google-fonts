module.exports = {
  name: 'multiple_fonts_loaded',
  css: `
.book_title {font-family: "cardo"}
p {font-family: "cormorantgaramond"}
`
}
