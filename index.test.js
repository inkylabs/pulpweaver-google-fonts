/* eslint-env jest */
const path = require('path')

const { runDir } = require('@inkylabs/pulpweaver/test')

// BEGIN tests ////////////////////////////////////////////////////////////////

runDir(path.join(__dirname, 'test/fixtures'))
