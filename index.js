const express = require('express')

const googleFonts = require('@inkylabs/google-fonts')
const siesta = require('@inkylabs/siesta')

const { generateCss } = require('./lib/css')

// BEGIN routines /////////////////////////////////////////////////////////////

function fontRoutine (data) {
  data.name = data.args.name
}

function googleFontsRoutine (data, parser) {
  parser.moduleData['google-fonts'].fonts =
      siesta.exciseDirectives(data, 'font')
}

// BEGIN layout ///////////////////////////////////////////////////////////////

function layoutDocument (data, parser) {
  const names = parser.moduleData['google-fonts'].fonts.map(f => f.name)
  const params = names.join('&')
  data.exportCss.push(`/module/google-fonts/fonts.css?${params}`)
  data.fonts.push(...names)
  return data.layout
}

// BEGIN router ///////////////////////////////////////////////////////////////

const router = express.Router()
router.route('/fonts.css').get(async (req, res) => {
  res.type('css')
    .status(200)
    .send(await generateCss(Object.keys(req.query)))
})
router.use('/fonts', express.static(googleFonts.dir()))

// BEGIN exports //////////////////////////////////////////////////////////////

module.exports = {
  name: 'google-fonts',
  directives: {
    'google-fonts': {
      routine: googleFontsRoutine,
      modules: [{
        name: 'google-fonts-font',
        directives: {
          font: {
            args: [{
              name: 'name'
            }],
            routine: fontRoutine
          }
        }
      }],
      // TODO: Support remove.
      remove: true
    }
  },
  layoutGenerators: {
    document: layoutDocument
  },
  router
}
